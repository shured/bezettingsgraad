#!/usr/bin/env perl 
#===============================================================================
#
#         FILE: actueel.pl
#        USAGE: ./actueel.pl  
#
#  DESCRIPTION: Dit script haalt uit de db bezettingsgraad.db de actuele stand
#  en plaatst dit in de tabel 'actueel';
#
#===============================================================================

use strict;
use warnings;
use utf8;
use LWP 5.64;
use DBI;
use Data::Dumper;

## db info
my $driver = "SQLite";
my $database = "bezettingsgraad.db";
my $dsn = "DBI:$driver:dbname=$database";
my $userid = "";
my $password = "";
my $dbh = DBI->connect($dsn, $userid, $password, { RaiseError => 1 }) or die $DBI::errstr;

# Stap 1 - Overzicht alle stallingen

my $Status=0;
my $select = "select distinct(naam),date('now') from bezettingsgraad where Datum = date('now') and (naam not like '%Hoog%' and naam not like '%Laag%') order by Naam desc;";
my $sth = $dbh->prepare($select);
$sth->execute();
while (my @rows = $sth->fetchrow_array) {
    my $select2 = "select vrij, bezettingsgraad from bezettingsgraad where naam = '$rows[0]' and Datum is date('now') order by Tijd desc;";
    my $stf = $dbh->prepare($select2);
    $stf->execute();
    my @rows2 = $stf->fetchrow_array;
    my $Vrij1 = $rows2[0];
    my $Vulgraad = $rows2[1];
    @rows2 = $stf->fetchrow_array;
    my $Vrij2 = $rows2[0];
    print ("Vrij1 $Vrij1 - Vrij2 $Vrij2 \n");
    if ( $Vrij1 > $Vrij2 ) {
        $Status = 0;  #Drukker
    } else {
       $Status = 1;   #Minder druk
    }
    my $insert="insert into actueel (`Naam`,`Datum`,`Vrij`,`Vulgraad`, `Status`) values ('$rows[0]','$rows[1]',$Vrij1, $Vulgraad, $Status);";
    my $stg= $dbh->prepare($insert);
    $stg->execute();
}


