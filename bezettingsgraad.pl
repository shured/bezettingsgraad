#!/usr/bin/env perl 
#===============================================================================
#
#         FILE: bezettingsgraad.pl
#        USAGE: ./bezettingsgraad.pl  
#
#  DESCRIPTION: Dit script haalt de huidge bezettingsgraad van de Utrechtse 
#  fietsenstallingen op bij stallingsnet.nl en plaatst dit in een csv.
#
#===============================================================================

use strict;
use warnings;
use utf8;
use LWP 5.64;
use DBI;

## db info
my $driver = "SQLite";
my $database = "bezettingsgraad.db";
my $dsn = "DBI:$driver:dbname=$database";
my $userid = "";
my $password = "";
my $dbh = DBI->connect($dsn, $userid, $password, { RaiseError => 1 }) or die $DBI::errstr;

my $browser = LWP::UserAgent->new ( agent => 'UtrechtBot/0.9 (+http://www.utrecht.nl)' );

my $api="https://stallingsnet.nl/api/1/parkingcount/utrecht";

my $content = $browser->get($api);
my $json = $content->{'_content'};

my @lines = split /{/, $json;

my ($FName, $facilityName, $totalPlaces, $freePlaces, $datum, $tijd);

foreach my $line (@lines) {
    my @fields = split /,/, $line;
    foreach my $field (@fields) {
          if (length($field) > 4 ) {
              if (substr($field,1,12) eq "facilityName") { ($FName,$facilityName) = split(/:/, $field);}
              if (substr($field,1,11) eq "totalPlaces") { ($FName,$totalPlaces) = split(/:/, $field);}
              if (substr($field,1,10) eq "freePlaces") { ($FName,$freePlaces) = split(/:/, $field);}
              if (substr($field,1,4) eq "time") { $tijd = substr($field,19,8);}
              if (substr($field,1,4) eq "time") { $datum = substr($field,8,10);}
              if (substr($field,1,12) eq "occupiedPlac") { 
                    my ($FName,$occupiedPlaces) = split(/:/, $field);
                    $occupiedPlaces =~ s/"//g;
                    $occupiedPlaces =~ s/]//g;
                    $occupiedPlaces =~ s/\}//g;
                    $tijd =~ s/"//g;
                    #print "Naam   : $facilityName\n";
                    #print "Totaal : $totalPlaces\n";
                    #print "Vrij   : $freePlaces\n";
                    #print "Bezet  : $occupiedPlaces\n"; 
                    #print "Tijd   : $datum - $tijd\n";
                    my $percentage = $occupiedPlaces/($totalPlaces/100);
                    $percentage = (int($percentage * 10)/10);
                    if ( $totalPlaces > 2 ) {
                       my $insert = qq(insert into bezettingsgraad (Naam,Datum, Tijd,Totaal,Vrij,Bezet,Bezettingsgraad) values ($facilityName,'$datum','$tijd',$totalPlaces,$freePlaces,$occupiedPlaces,$percentage));
                       my $rc = $dbh->do($insert) or die $DBI::errstr;
                    }
              }
          }
   }
}

