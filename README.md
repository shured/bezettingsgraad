# Bezettingsgraad

De gemeente Utrecht publiceert real-time data van de bezettingsgraad van de gemeentelijke fietsenstallingen in het centrum van Utrecht. Deze data is alleen real-time en mist history. Het perl script bezettingsgraad.pl trekt de data van de site en slaat dit op in een sqlite database. Het script actueel.pl vult vanuit de gegenereerde database een tabel met de laatste waardes per stalling en een (heel) korte analyze over de drukte.

Een actueel overzicht van de vrije plekken in de stallingen staat hier => http://opendata.shured.nl/Stallingen
Historische data (vanaf 17/10/2021) staat hier => http://opendata.shured.nl/bezettingsgraad.csv
